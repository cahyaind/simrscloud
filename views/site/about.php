<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="accordion" id="accordionExample">
  <div class="card">
    <div class="card-header" id="headingOne">
      <h2 class="mb-0">
        <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          Company Profile
        </button>
      </h2>
    </div>

    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
      <div class="card-body">
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Eligendi sit repellendus dolores vel molestiae. Ducimus pariatur placeat sequi eum impedit nobis excepturi nisi commodi numquam ullam necessitatibus modi rerum dolore, quam repellat ipsa omnis voluptatum quaerat magni, eos nostrum voluptas vero! Nulla quos iste eum eligendi cumque minima sunt quas.
    </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingTwo">
      <h2 class="mb-0">
        <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
          Visi dan Misi
        </button>
      </h2>
    </div>
    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
      <div class="card-body">
Lorem ipsum dolor sit amet consectetur adipisicing elit. Modi assumenda tempore fugit vero. Nulla ab iste dolor autem sunt, distinctio facere at aperiam! Iusto, quas.    </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingThree">
      <h2 class="mb-0">
        <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
          Lain-lain
        </button>
      </h2>
    </div>
    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
      <div class="card-body">
Lorem ipsum, dolor sit amet consectetur adipisicing elit. Dolor enim eum asperiores, reprehenderit quod doloremque. Debitis ducimus quam saepe error facilis earum facere sequi laboriosam assumenda labore magni est, eum quaerat voluptatibus, molestias fugit? Beatae sed iusto blanditiis fugit voluptas.    </div>
  </div>
</div>

    <!-- <code><?= __FILE__ ?></code> -->
</div>
