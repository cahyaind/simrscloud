<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "education".
 *
 * @property string $education_id
 * @property string $edu_name
 * @property int|null $s_order
 * @property string|null $created_by
 * @property string|null $created_time
 *
 * @property Patient[] $patients
 */
class Education extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'education';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['education_id', 'edu_name'], 'required'],
            [['s_order'], 'default', 'value' => null],
            [['s_order'], 'integer'],
            [['created_time'], 'safe'],
            [['education_id'], 'string', 'max' => 3],
            [['edu_name'], 'string', 'max' => 100],
            [['created_by'], 'string', 'max' => 20],
            [['education_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'education_id' => 'Education ID',
            'edu_name' => 'Edu Name',
            's_order' => 'S Order',
            'created_by' => 'Created By',
            'created_time' => 'Created Time',
        ];
    }

    /**
     * Gets query for [[Patients]].
     *
     * @return \yii\db\ActiveQuery|yii\db\ActiveQuery
     */
    public function getPatients()
    {
        return $this->hasMany(Patient::className(), ['education_id' => 'education_id']);
    }

    /**
     * {@inheritdoc}
     * @return EducationQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new EducationQuery(get_called_class());
    }
}
