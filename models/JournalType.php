<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "journal_type".
 *
 * @property int $jtype_id
 * @property int $hospital_id
 * @property string $code
 * @property string $name
 * @property string $created_by
 * @property string $created_time
 *
 * @property Hospital $hospital
 * @property Journal[] $journals
 */
class JournalType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'journal_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['hospital_id', 'code', 'name', 'created_by', 'created_time'], 'required'],
            [['hospital_id'], 'default', 'value' => null],
            [['hospital_id'], 'integer'],
            [['created_time'], 'safe'],
            [['code'], 'string', 'max' => 50],
            [['name'], 'string', 'max' => 100],
            [['created_by'], 'string', 'max' => 20],
            [['hospital_id'], 'exist', 'skipOnError' => true, 'targetClass' => Hospital::className(), 'targetAttribute' => ['hospital_id' => 'hospital_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'jtype_id' => 'Jtype ID',
            'hospital_id' => 'Hospital ID',
            'code' => 'Code',
            'name' => 'Name',
            'created_by' => 'Created By',
            'created_time' => 'Created Time',
        ];
    }

    /**
     * Gets query for [[Hospital]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getHospital()
    {
        return $this->hasOne(Hospital::className(), ['hospital_id' => 'hospital_id']);
    }

    /**
     * Gets query for [[Journals]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJournals()
    {
        return $this->hasMany(Journal::className(), ['jtype_id' => 'jtype_id']);
    }
}
