<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "blood_type".
 *
 * @property string $blood_id
 * @property string $name
 * @property int $s_order
 * @property string|null $created_by
 * @property string|null $created_time
 *
 * @property Patient[] $patients
 */
class BloodType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'blood_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['blood_id', 'name', 's_order'], 'required'],
            [['s_order'], 'default', 'value' => null],
            [['s_order'], 'integer'],
            [['created_time'], 'safe'],
            [['blood_id'], 'string', 'max' => 3],
            [['name'], 'string', 'max' => 100],
            [['created_by'], 'string', 'max' => 20],
            [['blood_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'blood_id' => 'Blood ID',
            'name' => 'Name',
            's_order' => 'S Order',
            'created_by' => 'Created By',
            'created_time' => 'Created Time',
        ];
    }

    /**
     * Gets query for [[Patients]].
     *
     * @return \yii\db\ActiveQuery|yii\db\ActiveQuery
     */
    public function getPatients()
    {
        return $this->hasMany(Patient::className(), ['blood_id' => 'blood_id']);
    }

    /**
     * {@inheritdoc}
     * @return BloodTypeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new BloodTypeQuery(get_called_class());
    }
}
