<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "room".
 *
 * @property int $room_id
 * @property int $hospital_id
 * @property int $clstroom_id
 * @property string $room_name
 * @property bool $is_active
 * @property bool $is_available
 * @property int|null $created_by
 * @property string|null $created_time
 * @property int|null $updated_by
 * @property string|null $updated_time
 *
 * @property Bed[] $beds
 * @property ClusterRoom $clstroom
 * @property Hospital $hospital
 */
class Room extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'room';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['hospital_id', 'clstroom_id', 'room_name'], 'required'],
            [['hospital_id', 'clstroom_id', 'created_by', 'updated_by'], 'default', 'value' => null],
            [['hospital_id', 'clstroom_id', 'created_by', 'updated_by'], 'integer'],
            [['is_active', 'is_available'], 'boolean'],
            [['created_time', 'updated_time'], 'safe'],
            [['room_name'], 'string', 'max' => 30],
            [['clstroom_id'], 'exist', 'skipOnError' => true, 'targetClass' => ClusterRoom::className(), 'targetAttribute' => ['clstroom_id' => 'clstroom_id']],
            [['hospital_id'], 'exist', 'skipOnError' => true, 'targetClass' => Hospital::className(), 'targetAttribute' => ['hospital_id' => 'hospital_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'room_id' => 'Room ID',
            'hospital_id' => 'Hospital ID',
            'clstroom_id' => 'Clstroom ID',
            'room_name' => 'Room Name',
            'is_active' => 'Is Active',
            'is_available' => 'Is Available',
            'created_by' => 'Created By',
            'created_time' => 'Created Time',
            'updated_by' => 'Updated By',
            'updated_time' => 'Updated Time',
        ];
    }

    /**
     * Gets query for [[Beds]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBeds()
    {
        return $this->hasMany(Bed::className(), ['room_id' => 'room_id']);
    }

    /**
     * Gets query for [[Clstroom]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getClstroom()
    {
        return $this->hasOne(ClusterRoom::className(), ['clstroom_id' => 'clstroom_id']);
    }

    /**
     * Gets query for [[Hospital]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getHospital()
    {
        return $this->hasOne(Hospital::className(), ['hospital_id' => 'hospital_id']);
    }
}
