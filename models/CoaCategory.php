<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "coa_category".
 *
 * @property string $ccat_code
 * @property string $ccat_name
 * @property bool $is_active
 * @property int|null $created_by
 * @property string|null $created_time
 * @property int|null $updated_by
 * @property string|null $updated_time
 * @property bool $is_deleted
 * @property int|null $deleted_by
 * @property string|null $deleted_time
 *
 * @property ChartOfAccount[] $chartOfAccounts
 * @property Users $createdBy
 * @property Users $deletedBy
 * @property Users $updatedBy
 */
class CoaCategory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'coa_category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ccat_code', 'ccat_name'], 'required'],
            [['is_active', 'is_deleted'], 'boolean'],
            [['created_by', 'updated_by', 'deleted_by'], 'default', 'value' => null],
            [['created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['created_time', 'updated_time', 'deleted_time'], 'safe'],
            [['ccat_code'], 'string', 'max' => 10],
            [['ccat_name'], 'string', 'max' => 100],
            [['ccat_code'], 'unique'],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['created_by' => 'user_id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['updated_by' => 'user_id']],
            [['deleted_by'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['deleted_by' => 'user_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ccat_code' => 'Ccat Code',
            'ccat_name' => 'Ccat Name',
            'is_active' => 'Is Active',
            'created_by' => 'Created By',
            'created_time' => 'Created Time',
            'updated_by' => 'Updated By',
            'updated_time' => 'Updated Time',
            'is_deleted' => 'Is Deleted',
            'deleted_by' => 'Deleted By',
            'deleted_time' => 'Deleted Time',
        ];
    }

    /**
     * Gets query for [[ChartOfAccounts]].
     *
     * @return \yii\db\ActiveQuery|yii\db\ActiveQuery
     */
    public function getChartOfAccounts()
    {
        return $this->hasMany(ChartOfAccount::className(), ['ccat_code' => 'ccat_code']);
    }

    /**
     * Gets query for [[CreatedBy]].
     *
     * @return \yii\db\ActiveQuery|UsersQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(Users::className(), ['user_id' => 'created_by']);
    }

    /**
     * Gets query for [[DeletedBy]].
     *
     * @return \yii\db\ActiveQuery|UsersQuery
     */
    public function getDeletedBy()
    {
        return $this->hasOne(Users::className(), ['user_id' => 'deleted_by']);
    }

    /**
     * Gets query for [[UpdatedBy]].
     *
     * @return \yii\db\ActiveQuery|UsersQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(Users::className(), ['user_id' => 'updated_by']);
    }

    /**
     * {@inheritdoc}
     * @return CoaCategoryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CoaCategoryQuery(get_called_class());
    }
}
