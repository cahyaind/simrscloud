<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "inventory_rate".
 *
 * @property int $invrate_id
 * @property int|null $hospital_id
 * @property int|null $inv_id
 * @property string|null $start_date
 * @property string|null $end_date
 * @property string|null $sk_num
 * @property float|null $suggested_factor
 * @property float|null $current_factor
 * @property float|null $purchasing_price
 * @property bool|null $is_active
 * @property int|null $created_by
 * @property string|null $created_time
 * @property int|null $updated_by
 * @property string|null $updated_time
 * @property bool|null $is_deleted
 * @property int|null $deleted_by
 * @property string|null $deleted_time
 *
 * @property Users $createdBy
 * @property Users $deletedBy
 * @property Hospital $hospital
 * @property Inventory $inv
 * @property Users $updatedBy
 */
class InventoryRate extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'inventory_rate';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['hospital_id', 'inv_id', 'created_by', 'updated_by', 'deleted_by'], 'default', 'value' => null],
            [['hospital_id', 'inv_id', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['start_date', 'end_date', 'created_time', 'updated_time', 'deleted_time'], 'safe'],
            [['suggested_factor', 'current_factor', 'purchasing_price'], 'number'],
            [['is_active', 'is_deleted'], 'boolean'],
            [['sk_num'], 'string', 'max' => 50],
            [['inv_id', 'hospital_id', 'is_active'], 'unique', 'targetAttribute' => ['inv_id', 'hospital_id', 'is_active']],
            [['hospital_id'], 'exist', 'skipOnError' => true, 'targetClass' => Hospital::className(), 'targetAttribute' => ['hospital_id' => 'hospital_id']],
            [['inv_id'], 'exist', 'skipOnError' => true, 'targetClass' => Inventory::className(), 'targetAttribute' => ['inv_id' => 'inv_id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['created_by' => 'user_id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['updated_by' => 'user_id']],
            [['deleted_by'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['deleted_by' => 'user_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'invrate_id' => 'Invrate ID',
            'hospital_id' => 'Hospital ID',
            'inv_id' => 'Inv ID',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
            'sk_num' => 'Sk Num',
            'suggested_factor' => 'Suggested Factor',
            'current_factor' => 'Current Factor',
            'purchasing_price' => 'Purchasing Price',
            'is_active' => 'Is Active',
            'created_by' => 'Created By',
            'created_time' => 'Created Time',
            'updated_by' => 'Updated By',
            'updated_time' => 'Updated Time',
            'is_deleted' => 'Is Deleted',
            'deleted_by' => 'Deleted By',
            'deleted_time' => 'Deleted Time',
        ];
    }

    /**
     * Gets query for [[CreatedBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(Users::className(), ['user_id' => 'created_by']);
    }

    /**
     * Gets query for [[DeletedBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDeletedBy()
    {
        return $this->hasOne(Users::className(), ['user_id' => 'deleted_by']);
    }

    /**
     * Gets query for [[Hospital]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getHospital()
    {
        return $this->hasOne(Hospital::className(), ['hospital_id' => 'hospital_id']);
    }

    /**
     * Gets query for [[Inv]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getInv()
    {
        return $this->hasOne(Inventory::className(), ['inv_id' => 'inv_id']);
    }

    /**
     * Gets query for [[UpdatedBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(Users::className(), ['user_id' => 'updated_by']);
    }
}
