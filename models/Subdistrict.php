<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "subdistrict".
 *
 * @property string $subdistrict_id
 * @property string $district_id
 * @property string $subdistrict_name
 * @property string|null $created_by
 * @property string|null $created_time
 * @property string|null $labelx
 *
 * @property District $district
 * @property Doctor[] $doctors
 * @property NonPatient[] $nonPatients
 * @property Patient[] $patients
 */
class Subdistrict extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'subdistrict';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['subdistrict_id', 'district_id', 'subdistrict_name'], 'required'],
            [['created_time'], 'safe'],
            [['subdistrict_id'], 'string', 'max' => 10],
            [['district_id'], 'string', 'max' => 6],
            [['subdistrict_name'], 'string', 'max' => 100],
            [['created_by'], 'string', 'max' => 20],
            [['labelx'], 'string', 'max' => 255],
            [['subdistrict_id'], 'unique'],
            [['district_id'], 'exist', 'skipOnError' => true, 'targetClass' => District::className(), 'targetAttribute' => ['district_id' => 'district_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'subdistrict_id' => 'Subdistrict ID',
            'district_id' => 'District ID',
            'subdistrict_name' => 'Subdistrict Name',
            'created_by' => 'Created By',
            'created_time' => 'Created Time',
            'labelx' => 'Labelx',
        ];
    }

    /**
     * Gets query for [[District]].
     *
     * @return \yii\db\ActiveQuery|DistrictQuery
     */
    public function getDistrict()
    {
        return $this->hasOne(District::className(), ['district_id' => 'district_id']);
    }

    /**
     * Gets query for [[Doctors]].
     *
     * @return \yii\db\ActiveQuery|yii\db\ActiveQuery
     */
    public function getDoctors()
    {
        return $this->hasMany(Doctor::className(), ['subdistrict_id' => 'subdistrict_id']);
    }

    /**
     * Gets query for [[NonPatients]].
     *
     * @return \yii\db\ActiveQuery|NonPatientQuery
     */
    public function getNonPatients()
    {
        return $this->hasMany(NonPatient::className(), ['subdistrict_id' => 'subdistrict_id']);
    }

    /**
     * Gets query for [[Patients]].
     *
     * @return \yii\db\ActiveQuery|yii\db\ActiveQuery
     */
    public function getPatients()
    {
        return $this->hasMany(Patient::className(), ['subdistrict_id' => 'subdistrict_id']);
    }

    /**
     * {@inheritdoc}
     * @return SubdistrictQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SubdistrictQuery(get_called_class());
    }
}
