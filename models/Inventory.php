<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "inventory".
 *
 * @property int $inv_id
 * @property int|null $hospital_id
 * @property string|null $name obat, barang, alkes dll
 * @property float|null $current_faktor presentase harga jual sekarang dengan harga beli terakhir
 * @property float|null $suggested_faktor presentase harga jual sekarang berdasarkan sk
 * @property string|null $internal_name
 *
 * @property InventoryRate[] $inventoryRates
 */
class Inventory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'inventory';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['hospital_id'], 'default', 'value' => null],
            [['hospital_id'], 'integer'],
            [['current_faktor', 'suggested_faktor'], 'number'],
            [['name', 'internal_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'inv_id' => 'Inv ID',
            'hospital_id' => 'Hospital ID',
            'name' => 'Name',
            'current_faktor' => 'Current Faktor',
            'suggested_faktor' => 'Suggested Faktor',
            'internal_name' => 'Internal Name',
        ];
    }

    /**
     * Gets query for [[InventoryRates]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getInventoryRates()
    {
        return $this->hasMany(InventoryRate::className(), ['inv_id' => 'inv_id']);
    }
}
