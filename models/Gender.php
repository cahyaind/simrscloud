<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "gender".
 *
 * @property string $gender_id
 * @property string $name
 * @property string|null $created_by
 * @property string|null $created_time
 *
 * @property Patient[] $patients
 */
class Gender extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'gender';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['gender_id', 'name'], 'required'],
            [['created_time'], 'safe'],
            [['gender_id'], 'string', 'max' => 2],
            [['name'], 'string', 'max' => 100],
            [['created_by'], 'string', 'max' => 20],
            [['gender_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'gender_id' => 'Gender ID',
            'name' => 'Name',
            'created_by' => 'Created By',
            'created_time' => 'Created Time',
        ];
    }

    /**
     * Gets query for [[Patients]].
     *
     * @return \yii\db\ActiveQuery|yii\db\ActiveQuery
     */
    public function getPatients()
    {
        return $this->hasMany(Patient::className(), ['gender_id' => 'gender_id']);
    }

    /**
     * {@inheritdoc}
     * @return GenderQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new GenderQuery(get_called_class());
    }
}
