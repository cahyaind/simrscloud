<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "journal".
 *
 * @property int $journal_id
 * @property int|null $hospital_id
 * @property string|null $registration_id
 * @property int|null $jtype_id
 * @property string $code
 * @property string $description
 * @property string $entry_date
 * @property bool|null $is_posting
 * @property string|null $user_posting
 * @property string|null $posting_date
 * @property string|null $posting_time
 * @property int|null $posting_shift
 * @property string $created_by
 * @property string $created_time
 * @property string|null $updated_by
 * @property string|null $updated_time
 *
 * @property Hospital $hospital
 * @property JournalDetail[] $journalDetails
 * @property JournalType $jtype
 */
class Journal extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'journal';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['hospital_id', 'jtype_id', 'posting_shift'], 'default', 'value' => null],
            [['hospital_id', 'jtype_id', 'posting_shift'], 'integer'],
            [['code', 'description', 'entry_date', 'created_by', 'created_time'], 'required'],
            [['description'], 'string'],
            [['entry_date', 'posting_date', 'posting_time', 'created_time', 'updated_time'], 'safe'],
            [['is_posting'], 'boolean'],
            [['registration_id'], 'string', 'max' => 10],
            [['code', 'user_posting', 'updated_by'], 'string', 'max' => 50],
            [['created_by'], 'string', 'max' => 20],
            [['hospital_id'], 'exist', 'skipOnError' => true, 'targetClass' => Hospital::className(), 'targetAttribute' => ['hospital_id' => 'hospital_id']],
            [['jtype_id'], 'exist', 'skipOnError' => true, 'targetClass' => JournalType::className(), 'targetAttribute' => ['jtype_id' => 'jtype_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'journal_id' => 'Journal ID',
            'hospital_id' => 'Hospital ID',
            'registration_id' => 'Registration ID',
            'jtype_id' => 'Jtype ID',
            'code' => 'Code',
            'description' => 'Description',
            'entry_date' => 'Entry Date',
            'is_posting' => 'Is Posting',
            'user_posting' => 'User Posting',
            'posting_date' => 'Posting Date',
            'posting_time' => 'Posting Time',
            'posting_shift' => 'Posting Shift',
            'created_by' => 'Created By',
            'created_time' => 'Created Time',
            'updated_by' => 'Updated By',
            'updated_time' => 'Updated Time',
        ];
    }

    /**
     * Gets query for [[Hospital]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getHospital()
    {
        return $this->hasOne(Hospital::className(), ['hospital_id' => 'hospital_id']);
    }

    /**
     * Gets query for [[JournalDetails]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJournalDetails()
    {
        return $this->hasMany(JournalDetail::className(), ['journal_id' => 'journal_id']);
    }

    /**
     * Gets query for [[Jtype]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJtype()
    {
        return $this->hasOne(JournalType::className(), ['jtype_id' => 'jtype_id']);
    }
}
