<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ProductRate;

/**
 * ProductRateSearch represents the model behind the search form of `app\models\ProductRate`.
 */
class ProductRateSearch extends ProductRate
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['prdrate_id', 'hospital_id', 'product_id', 'class_id', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['name', 'created_time', 'updated_time', 'deleted_time'], 'safe'],
            [['nominal'], 'number'],
            [['is_active', 'is_deleted'], 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ProductRate::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'prdrate_id' => $this->prdrate_id,
            'hospital_id' => $this->hospital_id,
            'product_id' => $this->product_id,
            'class_id' => $this->class_id,
            'nominal' => $this->nominal,
            'is_active' => $this->is_active,
            'created_by' => $this->created_by,
            'created_time' => $this->created_time,
            'updated_by' => $this->updated_by,
            'updated_time' => $this->updated_time,
            'is_deleted' => $this->is_deleted,
            'deleted_by' => $this->deleted_by,
            'deleted_time' => $this->deleted_time,
        ]);

        $query->andFilterWhere(['ilike', 'name', $this->name]);

        return $dataProvider;
    }
}
