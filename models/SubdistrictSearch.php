<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Subdistrict;

/**
 * SubdistrictSearch represents the model behind the search form of `app\models\Subdistrict`.
 */
class SubdistrictSearch extends Subdistrict
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['subdistrict_id', 'district_id', 'subdistrict_name', 'created_by', 'created_time', 'labelx'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Subdistrict::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'created_time' => $this->created_time,
        ]);

        $query->andFilterWhere(['ilike', 'subdistrict_id', $this->subdistrict_id])
            ->andFilterWhere(['ilike', 'district_id', $this->district_id])
            ->andFilterWhere(['ilike', 'subdistrict_name', $this->subdistrict_name])
            ->andFilterWhere(['ilike', 'created_by', $this->created_by])
            ->andFilterWhere(['ilike', 'labelx', $this->labelx]);

        return $dataProvider;
    }
}
