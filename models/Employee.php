<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "employee".
 *
 * @property int $person_id
 * @property string $employee_id
 * @property string $person_name
 * @property string $email
 * @property string $phone
 * @property string $date_of_bird
 * @property string $address
 * @property int $hospital_id
 * @property string|null $created_by
 * @property string|null $created_time
 *
 * @property Users $users
 */
class Employee extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'employee';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['employee_id', 'person_name', 'email', 'phone', 'date_of_bird', 'address', 'hospital_id'], 'required'],
            [['date_of_bird', 'created_time'], 'safe'],
            [['address'], 'string'],
            [['hospital_id'], 'default', 'value' => null],
            [['hospital_id'], 'integer'],
            [['employee_id', 'email'], 'string', 'max' => 50],
            [['person_name'], 'string', 'max' => 255],
            [['phone', 'created_by'], 'string', 'max' => 20],
            [['employee_id', 'hospital_id'], 'unique', 'targetAttribute' => ['employee_id', 'hospital_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'person_id' => 'Person ID',
            'employee_id' => 'Employee ID',
            'person_name' => 'Person Name',
            'email' => 'Email',
            'phone' => 'Phone',
            'date_of_bird' => 'Date Of Bird',
            'address' => 'Address',
            'hospital_id' => 'Hospital ID',
            'created_by' => 'Created By',
            'created_time' => 'Created Time',
        ];
    }

    /**
     * Gets query for [[Users]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasOne(Users::className(), ['person_id' => 'person_id']);
    }
}
