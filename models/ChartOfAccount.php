<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "chart_of_account".
 *
 * @property int $coa_id
 * @property int $hospital_id
 * @property string $ccat_code
 * @property string $coa_code
 * @property string $coa_name
 * @property int|null $parent
 * @property string $coa_description
 * @property bool $is_active
 * @property int|null $created_by
 * @property string|null $created_time
 * @property int|null $updated_by
 * @property string|null $updated_time
 * @property bool|null $is_deleted
 * @property int|null $deleted_by
 * @property string|null $deleted_time
 *
 * @property CoaCategory $ccatCode
 * @property Users $createdBy
 * @property Users $deletedBy
 * @property Hospital $hospital
 * @property Product[] $products
 * @property Users $updatedBy
 */
class ChartOfAccount extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'chart_of_account';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['hospital_id', 'ccat_code', 'coa_code', 'coa_name', 'coa_description'], 'required'],
            [['hospital_id', 'parent', 'created_by', 'updated_by', 'deleted_by'], 'default', 'value' => null],
            [['hospital_id', 'parent', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['coa_description'], 'string'],
            [['is_active', 'is_deleted'], 'boolean'],
            [['created_time', 'updated_time', 'deleted_time'], 'safe'],
            [['ccat_code', 'coa_code'], 'string', 'max' => 10],
            [['coa_name'], 'string', 'max' => 100],
            [['ccat_code'], 'exist', 'skipOnError' => true, 'targetClass' => CoaCategory::className(), 'targetAttribute' => ['ccat_code' => 'ccat_code']],
            [['hospital_id'], 'exist', 'skipOnError' => true, 'targetClass' => Hospital::className(), 'targetAttribute' => ['hospital_id' => 'hospital_id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['created_by' => 'user_id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['updated_by' => 'user_id']],
            [['deleted_by'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['deleted_by' => 'user_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'coa_id' => 'Coa ID',
            'hospital_id' => 'Hospital ID',
            'ccat_code' => 'Ccat Code',
            'coa_code' => 'Coa Code',
            'coa_name' => 'Coa Name',
            'parent' => 'Parent',
            'coa_description' => 'Coa Description',
            'is_active' => 'Is Active',
            'created_by' => 'Created By',
            'created_time' => 'Created Time',
            'updated_by' => 'Updated By',
            'updated_time' => 'Updated Time',
            'is_deleted' => 'Is Deleted',
            'deleted_by' => 'Deleted By',
            'deleted_time' => 'Deleted Time',
        ];
    }

    /**
     * Gets query for [[CcatCode]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCcatCode()
    {
        return $this->hasOne(CoaCategory::className(), ['ccat_code' => 'ccat_code']);
    }

    /**
     * Gets query for [[CreatedBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(Users::className(), ['user_id' => 'created_by']);
    }

    /**
     * Gets query for [[DeletedBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDeletedBy()
    {
        return $this->hasOne(Users::className(), ['user_id' => 'deleted_by']);
    }

    /**
     * Gets query for [[Hospital]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getHospital()
    {
        return $this->hasOne(Hospital::className(), ['hospital_id' => 'hospital_id']);
    }

    /**
     * Gets query for [[Products]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['coa_id' => 'coa_id']);
    }

    /**
     * Gets query for [[UpdatedBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(Users::className(), ['user_id' => 'updated_by']);
    }
}
