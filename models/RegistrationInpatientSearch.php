<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\RegistrationInpatient;

/**
 * RegistrationInpatientSearch represents the model behind the search form of `app\models\RegistrationInpatient`.
 */
class RegistrationInpatientSearch extends RegistrationInpatient
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['inpatien_id', 'registration_id', 'class_id', 'bed_id', 'created_by', 'updated_by'], 'integer'],
            [['is_active'], 'boolean'],
            [['date_in', 'time_in', 'date_out', 'time_out', 'created_time', 'updated_time'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RegistrationInpatient::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'inpatien_id' => $this->inpatien_id,
            'registration_id' => $this->registration_id,
            'class_id' => $this->class_id,
            'bed_id' => $this->bed_id,
            'is_active' => $this->is_active,
            'date_in' => $this->date_in,
            'time_in' => $this->time_in,
            'date_out' => $this->date_out,
            'time_out' => $this->time_out,
            'created_by' => $this->created_by,
            'created_time' => $this->created_time,
            'updated_by' => $this->updated_by,
            'updated_time' => $this->updated_time,
        ]);

        return $dataProvider;
    }
}
