<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Bed;

/**
 * BedSearch represents the model behind the search form of `app\models\Bed`.
 */
class BedSearch extends Bed
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['bed_id', 'hospital_id', 'room_id', 'last_used_by', 'created_by', 'updated_by'], 'integer'],
            [['bed_num', 'created_time', 'updated_time'], 'safe'],
            [['is_active', 'is_available'], 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Bed::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'bed_id' => $this->bed_id,
            'hospital_id' => $this->hospital_id,
            'room_id' => $this->room_id,
            'is_active' => $this->is_active,
            'is_available' => $this->is_available,
            'last_used_by' => $this->last_used_by,
            'created_by' => $this->created_by,
            'created_time' => $this->created_time,
            'updated_by' => $this->updated_by,
            'updated_time' => $this->updated_time,
        ]);

        $query->andFilterWhere(['ilike', 'bed_num', $this->bed_num]);

        return $dataProvider;
    }
}
