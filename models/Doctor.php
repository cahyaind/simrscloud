<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "doctor".
 *
 * @property int $doctor_id
 * @property int $hospital_id
 * @property string $nickname
 * @property string $fullname
 * @property string $address
 * @property string $subdistrict_id
 * @property string $handphone
 * @property bool $is_active
 * @property int|null $created_by
 * @property string|null $created_time
 * @property int|null $updated_by
 * @property string|null $updated_time
 *
 * @property Users $createdBy
 * @property Hospital $hospital
 * @property Registration[] $registrations
 * @property Subdistrict $subdistrict
 * @property Users $updatedBy
 */
class Doctor extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'doctor';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['hospital_id', 'nickname', 'fullname', 'address', 'subdistrict_id', 'handphone'], 'required'],
            [['hospital_id', 'created_by', 'updated_by'], 'default', 'value' => null],
            [['hospital_id', 'created_by', 'updated_by'], 'integer'],
            [['address'], 'string'],
            [['is_active'], 'boolean'],
            [['created_time', 'updated_time'], 'safe'],
            [['nickname'], 'string', 'max' => 50],
            [['fullname'], 'string', 'max' => 100],
            [['subdistrict_id'], 'string', 'max' => 10],
            [['handphone'], 'string', 'max' => 32],
            [['hospital_id'], 'exist', 'skipOnError' => true, 'targetClass' => Hospital::className(), 'targetAttribute' => ['hospital_id' => 'hospital_id']],
            [['subdistrict_id'], 'exist', 'skipOnError' => true, 'targetClass' => Subdistrict::className(), 'targetAttribute' => ['subdistrict_id' => 'subdistrict_id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['created_by' => 'user_id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['updated_by' => 'user_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'doctor_id' => 'Doctor ID',
            'hospital_id' => 'Hospital ID',
            'nickname' => 'Nickname',
            'fullname' => 'Fullname',
            'address' => 'Address',
            'subdistrict_id' => 'Subdistrict ID',
            'handphone' => 'Handphone',
            'is_active' => 'Is Active',
            'created_by' => 'Created By',
            'created_time' => 'Created Time',
            'updated_by' => 'Updated By',
            'updated_time' => 'Updated Time',
        ];
    }

    /**
     * Gets query for [[CreatedBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(Users::className(), ['user_id' => 'created_by']);
    }

    /**
     * Gets query for [[Hospital]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getHospital()
    {
        return $this->hasOne(Hospital::className(), ['hospital_id' => 'hospital_id']);
    }

    /**
     * Gets query for [[Registrations]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRegistrations()
    {
        return $this->hasMany(Registration::className(), ['doctor_id' => 'doctor_id']);
    }

    /**
     * Gets query for [[Subdistrict]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSubdistrict()
    {
        return $this->hasOne(Subdistrict::className(), ['subdistrict_id' => 'subdistrict_id']);
    }

    /**
     * Gets query for [[UpdatedBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(Users::className(), ['user_id' => 'updated_by']);
    }
}
