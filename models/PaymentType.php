<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "payment_type".
 *
 * @property string $paytype_id
 * @property string $coa_id
 * @property int $hospital_id
 * @property string $name
 * @property bool $is_active
 * @property string $description
 * @property bool|null $is_billing jika digunakan oleh kasir = true, jika gudang false
 *
 * @property Hospital $hospital
 * @property Payment[] $payments
 */
class PaymentType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'payment_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['paytype_id', 'coa_id', 'hospital_id', 'name', 'is_active', 'description'], 'required'],
            [['hospital_id'], 'default', 'value' => null],
            [['hospital_id'], 'integer'],
            [['is_active', 'is_billing'], 'boolean'],
            [['description'], 'string'],
            [['paytype_id'], 'string', 'max' => 3],
            [['coa_id'], 'string', 'max' => 10],
            [['name'], 'string', 'max' => 100],
            [['paytype_id'], 'unique'],
            [['hospital_id'], 'exist', 'skipOnError' => true, 'targetClass' => Hospital::className(), 'targetAttribute' => ['hospital_id' => 'hospital_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'paytype_id' => 'Paytype ID',
            'coa_id' => 'Coa ID',
            'hospital_id' => 'Hospital ID',
            'name' => 'Name',
            'is_active' => 'Is Active',
            'description' => 'Description',
            'is_billing' => 'Is Billing',
        ];
    }

    /**
     * Gets query for [[Hospital]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getHospital()
    {
        return $this->hasOne(Hospital::className(), ['hospital_id' => 'hospital_id']);
    }

    /**
     * Gets query for [[Payments]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPayments()
    {
        return $this->hasMany(Payment::className(), ['paytype_id' => 'paytype_id']);
    }
}
