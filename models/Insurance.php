<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "insurance".
 *
 * @property int $insurance_id
 * @property int $hospital_id
 * @property string $code
 * @property string $name
 * @property string $created_by
 * @property string $created_time
 *
 * @property Hospital $hospital
 * @property PatientInsurance[] $patientInsurances
 */
class Insurance extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'insurance';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['hospital_id', 'code', 'name', 'created_by', 'created_time'], 'required'],
            [['hospital_id'], 'default', 'value' => null],
            [['hospital_id'], 'integer'],
            [['created_time'], 'safe'],
            [['code'], 'string', 'max' => 50],
            [['name'], 'string', 'max' => 100],
            [['created_by'], 'string', 'max' => 20],
            [['hospital_id'], 'exist', 'skipOnError' => true, 'targetClass' => Hospital::className(), 'targetAttribute' => ['hospital_id' => 'hospital_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'insurance_id' => 'Insurance ID',
            'hospital_id' => 'Hospital ID',
            'code' => 'Code',
            'name' => 'Name',
            'created_by' => 'Created By',
            'created_time' => 'Created Time',
        ];
    }

    /**
     * Gets query for [[Hospital]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getHospital()
    {
        return $this->hasOne(Hospital::className(), ['hospital_id' => 'hospital_id']);
    }

    /**
     * Gets query for [[PatientInsurances]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPatientInsurances()
    {
        return $this->hasMany(PatientInsurance::className(), ['insurance_id' => 'insurance_id']);
    }
}
