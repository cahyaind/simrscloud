<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Manufactory */

$this->title = 'Update Manufactory: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Manufactories', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'factory_id' => $model->factory_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="manufactory-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
