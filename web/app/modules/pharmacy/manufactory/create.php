<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Manufactory */

$this->title = 'Create Manufactory';
$this->params['breadcrumbs'][] = ['label' => 'Manufactories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="manufactory-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
