<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\InventoryLargeUnit */

$this->title = 'Update Inventory Large Unit: ' . $model->largeunit_id;
$this->params['breadcrumbs'][] = ['label' => 'Inventory Large Units', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->largeunit_id, 'url' => ['view', 'largeunit_id' => $model->largeunit_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="inventory-large-unit-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
