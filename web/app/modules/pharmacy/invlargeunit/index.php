<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\InventoryLargeUnitSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Inventory Large Units';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inventory-large-unit-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Inventory Large Unit', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'largeunit_id',
            'large_unit',
            'is_active:boolean',
            'created_by',
            'created_time',
            //'updated_by',
            //'updated_time',
            //'is_deleted:boolean',
            //'deleted_by',
            //'deleted_time',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
