<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\InventoryGroup */

$this->title = 'Update Inventory Group: ' . $model->invgroup_id;
$this->params['breadcrumbs'][] = ['label' => 'Inventory Groups', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->invgroup_id, 'url' => ['view', 'invgroup_id' => $model->invgroup_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="inventory-group-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
