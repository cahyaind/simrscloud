<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\InventorySmallUnit */

$this->title = 'Update Inventory Small Unit: ' . $model->smallunit_id;
$this->params['breadcrumbs'][] = ['label' => 'Inventory Small Units', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->smallunit_id, 'url' => ['view', 'smallunit_id' => $model->smallunit_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="inventory-small-unit-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
