<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\InventorySmallUnit */

$this->title = 'Create Inventory Small Unit';
$this->params['breadcrumbs'][] = ['label' => 'Inventory Small Units', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inventory-small-unit-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
