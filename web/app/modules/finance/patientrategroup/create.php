<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PatientRateGroup */

$this->title = 'Create Patient Rate Group';
$this->params['breadcrumbs'][] = ['label' => 'Patient Rate Groups', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="patient-rate-group-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
