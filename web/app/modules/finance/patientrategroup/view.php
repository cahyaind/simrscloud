<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\PatientRateGroup */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Patient Rate Groups', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="patient-rate-group-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'prg_id' => $model->prg_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'prg_id' => $model->prg_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'prg_id',
            'name',
            'created_by',
            'created_time',
        ],
    ]) ?>

</div>
