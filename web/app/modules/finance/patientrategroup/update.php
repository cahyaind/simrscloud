<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PatientRateGroup */

$this->title = 'Update Patient Rate Group: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Patient Rate Groups', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'prg_id' => $model->prg_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="patient-rate-group-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
