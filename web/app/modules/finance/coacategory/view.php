<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\CoaCategory */

$this->title = $model->ccat_code;
$this->params['breadcrumbs'][] = ['label' => 'Coa Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="coa-category-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'ccat_code' => $model->ccat_code], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'ccat_code' => $model->ccat_code], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'ccat_code',
            'ccat_name',
            'is_active:boolean',
            'created_by',
            'created_time',
            'updated_by',
            'updated_time',
            'is_deleted:boolean',
            'deleted_by',
            'deleted_time',
        ],
    ]) ?>

</div>
