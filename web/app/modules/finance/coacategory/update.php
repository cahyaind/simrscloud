<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CoaCategory */

$this->title = 'Update Coa Category: ' . $model->ccat_code;
$this->params['breadcrumbs'][] = ['label' => 'Coa Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ccat_code, 'url' => ['view', 'ccat_code' => $model->ccat_code]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="coa-category-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
