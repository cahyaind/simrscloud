<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CoaCategory */

$this->title = 'Create Coa Category';
$this->params['breadcrumbs'][] = ['label' => 'Coa Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="coa-category-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
