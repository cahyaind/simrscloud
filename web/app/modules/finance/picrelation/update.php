<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PicRelation */

$this->title = 'Update Pic Relation: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Pic Relations', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'picrel_id' => $model->picrel_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pic-relation-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
