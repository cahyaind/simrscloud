<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PicRelation */

$this->title = 'Create Pic Relation';
$this->params['breadcrumbs'][] = ['label' => 'Pic Relations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pic-relation-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
