<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProductRate */

$this->title = 'Create Product Rate';
$this->params['breadcrumbs'][] = ['label' => 'Product Rates', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-rate-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
