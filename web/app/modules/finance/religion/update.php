<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Religion */

$this->title = 'Update Religion: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Religions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'religion_id' => $model->religion_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="religion-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
