<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RegInpatientSource */

$this->title = 'Update Reg Inpatient Source: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Reg Inpatient Sources', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'regsource_id' => $model->regsource_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="reg-inpatient-source-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
