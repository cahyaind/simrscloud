<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RegInpatientSource */

$this->title = 'Create Reg Inpatient Source';
$this->params['breadcrumbs'][] = ['label' => 'Reg Inpatient Sources', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reg-inpatient-source-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
