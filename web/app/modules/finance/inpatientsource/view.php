<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\RegInpatientSource */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Reg Inpatient Sources', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="reg-inpatient-source-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'regsource_id' => $model->regsource_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'regsource_id' => $model->regsource_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'regsource_id',
            'name',
            'created_by',
            'created_time',
        ],
    ]) ?>

</div>
