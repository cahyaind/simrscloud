<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RegInpatientSourceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Reg Inpatient Sources';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reg-inpatient-source-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Reg Inpatient Source', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'regsource_id',
            'name',
            'created_by',
            'created_time',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
