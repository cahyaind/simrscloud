<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\UnitGroup */

$this->title = 'Update Unit Group: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Unit Groups', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'unitgroup_id' => $model->unitgroup_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="unit-group-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
