<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\UnitGroup */

$this->title = 'Create Unit Group';
$this->params['breadcrumbs'][] = ['label' => 'Unit Groups', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="unit-group-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
