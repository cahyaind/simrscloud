<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\RegistrationStatus */

$this->title = $model->regstts_id;
$this->params['breadcrumbs'][] = ['label' => 'Registration Statuses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="registration-status-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'regstts_id' => $model->regstts_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'regstts_id' => $model->regstts_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'regstts_id',
            'reg_status',
            'created_by',
            'created_time',
        ],
    ]) ?>

</div>
