<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RegistrationStatus */

$this->title = 'Update Registration Status: ' . $model->regstts_id;
$this->params['breadcrumbs'][] = ['label' => 'Registration Statuses', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->regstts_id, 'url' => ['view', 'regstts_id' => $model->regstts_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="registration-status-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
