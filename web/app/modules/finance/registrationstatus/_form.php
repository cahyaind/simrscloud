<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\RegistrationStatus */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="registration-status-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'regstts_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'reg_status')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_by')->textInput() ?>

    <?= $form->field($model, 'created_time')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
