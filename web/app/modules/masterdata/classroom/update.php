<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ClassRoom */

$this->title = 'Update Class Room: ' . $model->class_id;
$this->params['breadcrumbs'][] = ['label' => 'Class Rooms', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->class_id, 'url' => ['view', 'class_id' => $model->class_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="class-room-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
