<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\PatientGuaranty */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Patient Guaranties', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="patient-guaranty-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'guaranty_id' => $model->guaranty_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'guaranty_id' => $model->guaranty_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'guaranty_id',
            'hospital_id',
            'name',
            'pic_name',
            'address:ntext',
            'phone_number',
            'email:email',
            'note:ntext',
            'is_active:boolean',
            'created_by',
            'created_time',
            'updated_by',
            'updated_time',
        ],
    ]) ?>

</div>
