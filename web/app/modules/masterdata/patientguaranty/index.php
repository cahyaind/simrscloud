<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\PatientGuarantySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Patient Guaranties';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="patient-guaranty-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Patient Guaranty', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'guaranty_id',
            'hospital_id',
            'name',
            'pic_name',
            'address:ntext',
            //'phone_number',
            //'email:email',
            //'note:ntext',
            //'is_active:boolean',
            //'created_by',
            //'created_time',
            //'updated_by',
            //'updated_time',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
