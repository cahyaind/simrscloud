<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PatientGuaranty */

$this->title = 'Update Patient Guaranty: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Patient Guaranties', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'guaranty_id' => $model->guaranty_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="patient-guaranty-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
