<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BedSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Beds';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bed-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Bed', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'bed_id',
            'hospital_id',
            'room_id',
            'bed_num',
            'is_active:boolean',
            //'is_available:boolean',
            //'last_used_by',
            //'created_by',
            //'created_time',
            //'updated_by',
            //'updated_time',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
