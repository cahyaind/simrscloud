<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ClusterRoom */

$this->title = 'Create Cluster Room';
$this->params['breadcrumbs'][] = ['label' => 'Cluster Rooms', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cluster-room-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
