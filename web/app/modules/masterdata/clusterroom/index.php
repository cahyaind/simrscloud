<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ClusterRoomSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cluster Rooms';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cluster-room-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Cluster Room', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'clstroom_id',
            'hospital_id',
            'cls_room',
            'is_active:boolean',
            'created_by',
            //'created_time',
            //'updated_by',
            //'updated_time',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
