<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ClusterRoom */

$this->title = $model->clstroom_id;
$this->params['breadcrumbs'][] = ['label' => 'Cluster Rooms', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="cluster-room-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'clstroom_id' => $model->clstroom_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'clstroom_id' => $model->clstroom_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'clstroom_id',
            'hospital_id',
            'cls_room',
            'is_active:boolean',
            'created_by',
            'created_time',
            'updated_by',
            'updated_time',
        ],
    ]) ?>

</div>
