<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ClusterRoom */

$this->title = 'Update Cluster Room: ' . $model->clstroom_id;
$this->params['breadcrumbs'][] = ['label' => 'Cluster Rooms', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->clstroom_id, 'url' => ['view', 'clstroom_id' => $model->clstroom_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="cluster-room-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
