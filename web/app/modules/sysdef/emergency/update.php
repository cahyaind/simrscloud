<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Emergency */

$this->title = 'Update Emergency: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Emergencies', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'emergency_id' => $model->emergency_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="emergency-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
