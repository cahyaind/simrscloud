<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Emergency */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Emergencies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="emergency-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'emergency_id' => $model->emergency_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'emergency_id' => $model->emergency_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'emergency_id',
            'name',
            's_order',
            'created_by',
            'created_time',
            'updated_by',
            'updated_time',
        ],
    ]) ?>

</div>
