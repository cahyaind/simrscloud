<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\BloodType */

$this->title = 'Update Blood Type: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Blood Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'blood_id' => $model->blood_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="blood-type-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
