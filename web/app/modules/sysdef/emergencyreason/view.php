<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\EmergencyReason */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Emergency Reasons', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="emergency-reason-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'reason_id' => $model->reason_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'reason_id' => $model->reason_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'reason_id',
            'name',
            's_order',
            'created_by',
            'created_time',
            'updated_by',
            'updated_time',
        ],
    ]) ?>

</div>
