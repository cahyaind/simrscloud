<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\EmergencyReason */

$this->title = 'Create Emergency Reason';
$this->params['breadcrumbs'][] = ['label' => 'Emergency Reasons', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="emergency-reason-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
