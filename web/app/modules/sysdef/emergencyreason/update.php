<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\EmergencyReason */

$this->title = 'Update Emergency Reason: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Emergency Reasons', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'reason_id' => $model->reason_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="emergency-reason-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
