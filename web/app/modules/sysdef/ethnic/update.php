<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Ethnic */

$this->title = 'Update Ethnic: ' . $model->ethnic_id;
$this->params['breadcrumbs'][] = ['label' => 'Ethnics', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ethnic_id, 'url' => ['view', 'ethnic_id' => $model->ethnic_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ethnic-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
