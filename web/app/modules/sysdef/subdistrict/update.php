<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Subdistrict */

$this->title = 'Update Subdistrict: ' . $model->subdistrict_id;
$this->params['breadcrumbs'][] = ['label' => 'Subdistricts', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->subdistrict_id, 'url' => ['view', 'subdistrict_id' => $model->subdistrict_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="subdistrict-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
