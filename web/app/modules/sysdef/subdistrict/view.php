<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Subdistrict */

$this->title = $model->subdistrict_id;
$this->params['breadcrumbs'][] = ['label' => 'Subdistricts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="subdistrict-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'subdistrict_id' => $model->subdistrict_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'subdistrict_id' => $model->subdistrict_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'subdistrict_id',
            'district_id',
            'subdistrict_name',
            'created_by',
            'created_time',
            'labelx',
        ],
    ]) ?>

</div>
