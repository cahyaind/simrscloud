<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RegistrationInpatient */

$this->title = 'Create Registration Inpatient';
$this->params['breadcrumbs'][] = ['label' => 'Registration Inpatients', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="registration-inpatient-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
