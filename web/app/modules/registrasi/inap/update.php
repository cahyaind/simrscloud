<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RegistrationInpatient */

$this->title = 'Update Registration Inpatient: ' . $model->inpatien_id;
$this->params['breadcrumbs'][] = ['label' => 'Registration Inpatients', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->inpatien_id, 'url' => ['view', 'inpatien_id' => $model->inpatien_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="registration-inpatient-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
