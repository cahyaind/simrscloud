<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\RegistrationInpatient */

$this->title = $model->inpatien_id;
$this->params['breadcrumbs'][] = ['label' => 'Registration Inpatients', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="registration-inpatient-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'inpatien_id' => $model->inpatien_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'inpatien_id' => $model->inpatien_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'inpatien_id',
            'registration_id',
            'class_id',
            'bed_id',
            'is_active:boolean',
            'date_in',
            'time_in',
            'date_out',
            'time_out',
            'created_by',
            'created_time',
            'updated_by',
            'updated_time',
        ],
    ]) ?>

</div>
