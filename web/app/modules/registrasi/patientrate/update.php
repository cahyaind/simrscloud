<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PatientRate */

$this->title = 'Update Patient Rate: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Patient Rates', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'rate_id' => $model->rate_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="patient-rate-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
