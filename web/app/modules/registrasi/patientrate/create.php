<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PatientRate */

$this->title = 'Create Patient Rate';
$this->params['breadcrumbs'][] = ['label' => 'Patient Rates', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="patient-rate-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
