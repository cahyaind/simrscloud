<?php

use yii\helpers\Url;
use mdm\admin\components\MenuHelper;
use yii\helpers\Html;

$callback = function ($menu) {
    $data = eval($menu['data']);
    return [
            'label' => $menu['name'],
            'url' => [$menu['route']],
            'options' => $data,
            'items' => $menu['children']
    ];
};
$menu = MenuHelper::getAssignedMenu(Yii::$app->user->id);
//echo '<pre>';
//var_dump($menu);
//exit;
?>
<div class="sticky">
    <div class="horizontal-main hor-menu clearfix">
        <div class="horizontal-mainwrapper container clearfix">
            <!--Nav-->
            <nav class="horizontalMenu clearfix">
                <ul class="horizontalMenu-list">
                    <?php
                    foreach ($menu as $row) {
                        if (isset($row['items'])) {
                            echo '<li aria-haspopup="true">';
                            echo Html::a('<i class="' . $row['icon'] . '"></i>' . $row['label'] . ' <i class="fa fa-angle-down horizontal-icon" ></i >', Url::toRoute([$row['url'][0]]), ['class' => 'sub-icon']);
                            echo '<ul class="sub-menu">';
                            foreach ($row['items'] as $val) {
                                if (isset($val['items'])) {
                                    echo '<li aria-haspopup="true">';
                                    echo Html::a('<i class="' . $val['icon'] . '"></i>' . $val['label'], Url::toRoute([$val['url'][0]]), ['class' => 'sub-menu-sub']);
                                    echo '<ul class="sub-menu">';
                                    foreach ($val['items'] as $xval) {
                                        echo '<li aria-haspopup="true">' . Html::a('<i class="' . $xval['icon'] . '"></i> ' . $xval['label'], Url::toRoute([$xval['url'][0]]), ['class' => 'slide-item']) . '</li>';
                                    }
                                    echo '</ul>';
                                    echo '<li>';
                                } else {
                                    echo '<li aria-haspopup="true">' . Html::a('<i class="' . $val['icon'] . '"></i> ' . $val['label'], Url::toRoute([$val['url'][0]]), ['class' => 'slide-item']) . '</li>';
                                }
                            }
                            echo '</ul>';
                            echo '<li>';
                        } else {
                            echo '<li aria-haspopup="true">' . Html::a('<i class="' . $row['icon'] . '"></i>' . $row['label'], Url::toRoute([$row['url'][0]])) . '</li>';
                        }
                    }
                    ?>
                </ul>
            </nav>
            <!--Nav-->
        </div>
    </div>
</div>