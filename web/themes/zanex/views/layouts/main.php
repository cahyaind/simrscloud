<?php

use app\assets\ZanexAsset;
use yii\helpers\Html;
use yii\helpers\Url;

ZanexAsset::register($this);
$this->beginPage();
?>
    <!doctype html>
    <html lang="en" dir="ltr">
    <head>

        <!-- META DATA -->
        <meta charset="UTF-8">
        <meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=0'>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="Zanex – Bootstrap  Admin & Dashboard Template">
        <meta name="author" content="Spruko Technologies Private Limited">
        <meta name="keywords"
              content="admin, dashboard, dashboard ui, admin dashboard template, admin panel dashboard, admin panel html, admin panel html template, admin panel template, admin ui templates, administrative templates, best admin dashboard, best admin templates, bootstrap 4 admin template, bootstrap admin dashboard, bootstrap admin panel, html css admin templates, html5 admin template, premium bootstrap templates, responsive admin template, template admin bootstrap 4, themeforest html">

        <!-- FAVICON -->
        <link rel="shortcut icon" type="image/x-icon"
              href="<?= $this->theme->baseUrl ?>/assets/images/brand/favicon.ico"/>

        <!-- TITLE -->
        <title><?= Yii::$app->name ?></title>
        <?php $this->head() ?>
        <?php $this->registerCsrfMetaTags() ?>
        <style>
            /*-----Feather icons-----*/
            @font-face {
                font-family: "feather" !important;
                src: url("<?= $this->theme->baseUrl?>/fonts/feather/feather-webfont.eot?t=1501841394106") !important;
                /* IE9*/
                src: url("<?= $this->theme->baseUrl?>/fonts/feather/feather-webfont.eot?t=1501841394106#iefix") format("embedded-opentype"), url("<?= $this->theme->baseUrl?>/fonts/feather/feather-webfont.woff?t=1501841394106") format("woff"), url("<?= $this->theme->baseUrl?>/fonts/feather/feather-webfont.ttf?t=1501841394106") format("truetype"), url("<?= $this->theme->baseUrl?>/fonts/feather/feather-webfont.svg?t=1501841394106#feather") format("svg") !important;
                /* iOS 4.1- */
            }
        </style>
    </head>
    <body>
    <?php $this->beginBody() ?>
    <!-- GLOBAL-LOADER -->
    <div id="global-loader">
        <img src="<?= $this->theme->baseUrl ?>/assets/images/loader.svg" class="loader-img" alt="Loader">
    </div>
    <div class="modal fade" id="loadmodal">
        <div class="modal-dialog modal-dialog-centered text-center " role="document">
            <div class="modal-content tx-size-sm">
                <div id="loadformloader"></div>
                <div class="modal-body text-center p-4" id="loadformcontent"></div>
            </div>
        </div>
    </div>
    <!-- /GLOBAL-LOADER -->
    <!-- PAGE -->
    <div class="page">
        <div class="page-main">
            <!-- HEADER -->
            <div class="header hor-header">
                <div class="container">
                    <div class="d-flex">
                        <a class="animated-arrow hor-toggle horizontal-navtoggle"><span></span></a>
                        <a class="header-brand1" href="index.html">
                            <img src="<?= $this->theme->baseUrl ?>/assets/images/brand/logo-3.png"
                                 class="header-brand-img desktop-logo" alt="logo">
                            <img src="<?= $this->theme->baseUrl ?>/assets/images/brand/logo.png"
                                 class="header-brand-img light-logo" alt="logo">
                        </a><!-- LOGO -->
                        <div class="main-header-center ms-3 d-none d-md-block">
                            <input class="form-control" placeholder="Search for anything..." type="search">
                            <button class="btn"><i class="fa fa-search" aria-hidden="true"></i></button>
                        </div>
                        <div class="d-flex order-lg-2 ms-auto header-right-icons">
                            <div class="dropdown d-lg-none d-md-block d-none">
                                <a href="#" class="nav-link icon" data-bs-toggle="dropdown">
                                    <i class="fe fe-search"></i>
                                </a>
                                <div class="dropdown-menu header-search dropdown-menu-start">
                                    <div class="input-group w-100 p-2">
                                        <input type="text" class="form-control" placeholder="Search....">
                                        <div class="input-group-text btn btn-primary">
                                            <i class="fa fa-search" aria-hidden="true"></i>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- SEARCH -->
                            <button class="navbar-toggler navresponsive-toggler d-md-none ms-auto" type="button"
                                    data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent-4"
                                    aria-controls="navbarSupportedContent-4" aria-expanded="false"
                                    aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon fe fe-more-vertical text-dark"></span>
                            </button>
                            <div class="dropdown d-none d-md-flex">
                                <a class="nav-link icon theme-layout nav-link-bg layout-setting">
                                <span class="dark-layout" data-bs-placement="bottom" data-bs-toggle="tooltip"
                                      title="Dark Theme"><i class="fe fe-moon"></i></span>
                                    <span class="light-layout" data-bs-placement="bottom" data-bs-toggle="tooltip"
                                          title="Light Theme"><i class="fe fe-sun"></i></span>
                                </a>
                            </div><!-- Theme-Layout -->
                            <div class="dropdown d-none d-md-flex">
                                <a class="nav-link icon full-screen-link nav-link-bg">
                                    <i class="fe fe-minimize fullscreen-button"></i>
                                </a>
                            </div>
                            <div class="dropdown d-none d-md-flex profile-1">
                                <a href="#" data-bs-toggle="dropdown" class="nav-link pe-2 leading-none d-flex">
                                    <span> <img src="<?= $this->theme->baseUrl ?>/assets/images/users/8.jpg"
                                                alt="profile-user"
                                                class="avatar  profile-user brround cover-image"> </span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
                                    <div class="drop-heading">
                                        <div class="text-center">
                                            <h5 class="text-dark mb-0"><?= Yii::$app->user->identity->employee->person_name ?></h5>
                                            <small class="text-muted"><?= Yii::$app->user->identity->username ?></small>
                                        </div>
                                    </div>
                                    <div class="dropdown-divider m-0"></div>
                                    <a class="dropdown-item"
                                       href="<?= Url::toRoute(['/masterdata/user/view', 'id' => Yii::$app->user->getId()]) ?>">
                                        <i class="dropdown-icon fe fe-user"></i> Profile
                                    </a>
                                    <a class="dropdown-item" href="email.html">
                                        <i class="dropdown-icon fe fe-mail"></i> Inbox
                                        <span class="badge bg-primary float-end">3</span>
                                    </a>
                                    <a class="dropdown-item" href="emailservices.html">
                                        <i class="dropdown-icon fe fe-settings"></i> Settings
                                    </a>
                                    <a class="dropdown-item" href="faq.html">
                                        <i class="dropdown-icon fe fe-alert-triangle"></i> Need help??
                                    </a>
                                    <a class="dropdown-item" href="<?= Url::toRoute(['/site/logout']) ?>">
                                        <i class="dropdown-icon fe fe-alert-circle"></i> Sign out
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End HEADER -->
            <!--/Horizontal-main -->
            <?= $this->render('_menu') ?>
            <!--/Horizontal-main -->
            <!--app-content open-->
            <div class="app-content hor-content">
                <div class="container" style="margin-top: 5px;">
                    <?= $content ?>
                </div>
                <!-- CONTAINER CLOSED -->
            </div>
        </div>
    </div>
    <?php
    $this->endBody();
    $varData = \yii\helpers\Json::encode([\yii::$app->request->csrfParam => \yii::$app->request->csrfToken]);
    $ajaxSetup = <<<JS
        $.ajaxSetup({ data: $varData });
        $('input').attr('autocomplete','off');
JS;
    $this->registerJs($ajaxSetup);
    ?>
    </body>
    </html>
<?php $this->endPage(); ?>