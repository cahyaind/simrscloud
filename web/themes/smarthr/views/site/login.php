<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
	<meta name="description" content="Smarthr - Bootstrap Admin Template">
	<meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern, accounts, invoice, html5, responsive, CRM, Projects">
	<meta name="author" content="Dreamguys - Bootstrap Admin Template">
	<meta name="robots" content="noindex, nofollow">
	<title>Login - HRMS admin template</title>

	<!-- Favicon -->
	<link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.png">

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="<?= $this->theme->baseUrl ?>assets/css/bootstrap.min.css">

	<!-- Fontawesome CSS -->
	<link rel="stylesheet" href="<?= $this->theme->baseUrl ?>assets/css/font-awesome.min.css">

	<!-- Main CSS -->
	<link rel="stylesheet" href="assets/css/style.css">

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
			<script src="assets/js/html5shiv.min.js"></script>
			<script src="assets/js/respond.min.js"></script>
		<![endif]-->
</head>

<body class="account-page">

	<!-- Main Wrapper -->
	<div class="main-wrapper">
		<div class="account-content">
			<!-- CONTAINER OPEN -->
			<div class="col col-login mx-auto">
				<div class="text-center">
					<img src="<?= $this->theme->baseUrl ?>/assets/images/brand/logo.png" class="header-brand-img" alt="">
				</div>
			</div>
			<div class="container-login100">
				<div class="wrap-login100 p-0">
					<div class="card-body">
						<?php $form = ActiveForm::begin(['id' => 'login-form', 'options' => ['class' => 'login100-form validate-form']]); ?>
						<span class="login100-form-title">
							Login
						</span>
						<div class="wrap-input100 validate-input" data-bs-validate="Valid email is required: ex@abc.xyz">
							<?= $form->field($model, 'username')->textInput(['autofocus' => true, 'class' => 'input100'])->label(false) ?>
							<span class="focus-input100"></span>
							<span class="symbol-input100">
								<i class="zmdi zmdi-email" aria-hidden="true"></i>
							</span>
						</div>
						<div class="wrap-input100 validate-input" data-bs-validate="Password is required">
							<?= $form->field($model, 'password')->passwordInput(['class' => 'input100'])->label(false) ?>
							<span class="focus-input100"></span>
							<span class="symbol-input100">
								<i class="zmdi zmdi-lock" aria-hidden="true"></i>
							</span>
						</div>
						<div class="container-login100-form-btn">
							<?= Html::submitButton('Login', ['class' => 'login100-form-btn btn-primary', 'name' => 'login-button']) ?>
						</div>
						<?php ActiveForm::end(); ?>
					</div>

				</div>
			</div>
			<!-- CONTAINER CLOSED -->
		</div>
	</div>
	<!-- /Main Wrapper -->

	<!-- jQuery -->
	<script src="<?= $this->theme->baseUrl ?>/assets/js/jquery-3.5.1.min.js"></script>

	<!-- Bootstrap Core JS -->
	<script src="<?= $this->theme->baseUrl ?>/assets/js/popper.min.js"></script>
	<script src="<?= $this->theme->baseUrl ?>/assets/js/bootstrap.min.js"></script>

	<!-- Custom JS -->
	<script src="<?= $this->theme->baseUrl ?>/assets/js/app.js"></script>

</body>

</html>