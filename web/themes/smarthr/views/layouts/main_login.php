<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\assets\AppAsset;
use app\widgets\Alert;
use yii\bootstrap4\Breadcrumbs;
use yii\bootstrap4\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;

use mdm\admin\components\MenuHelper;

AppAsset::register($this);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <meta name="description" content="Smarthr - Bootstrap Admin Template">
    <meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern, accounts, invoice, html5, responsive, CRM, Projects">
    <meta name="author" content="Dreamguys - Bootstrap Admin Template">
    <meta name="robots" content="noindex, nofollow">
    <title>Simrscloud</title>

    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo $this->theme->baseUrl ?>/assets/img/favicon.png">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo $this->theme->baseUrl ?>/assets/css/bootstrap.min.css">

    <!-- Fontawesome CSS -->
    <link rel="stylesheet" href="<?php echo $this->theme->baseUrl ?>/assets/css/font-awesome.min.css">

    <!-- Lineawesome CSS -->
    <link rel="stylesheet" href="<?php echo $this->theme->baseUrl ?>/assets/css/line-awesome.min.css">

    <!-- Main CSS -->
    <link rel="stylesheet" href="<?php echo $this->theme->baseUrl ?>/assets/css/style.css">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
            <script src="assets/js/html5shiv.min.js"></script>
            <script src="assets/js/respond.min.js"></script>
        <![endif]-->
</head>

<body>
    <!-- Main Wrapper -->
    <div class="main-wrapper">

        <!-- Content Starts -->
        <?php echo $content ?>
        <!-- /Content End -->

    </div>
    <!-- /Page Content -->

    </div>
    <!-- /Page Wrapper -->

    </div>
    <!-- /Main Wrapper -->

    <!-- jQuery -->
    <script src="<?php echo $this->theme->baseUrl ?>/assets/js/jquery-3.5.1.min.js"></script>

    <!-- Bootstrap Core JS -->
    <script src="<?php echo $this->theme->baseUrl ?>/assets/js/popper.min.js"></script>
    <script src="<?php echo $this->theme->baseUrl ?>/assets/js/bootstrap.min.js"></script>

    <!-- Slimscroll JS -->
    <script src="<?php echo $this->theme->baseUrl ?>/assets/js/jquery.slimscroll.min.js"></script>

    <!-- Chart JS -->
    <script src="<?php echo $this->theme->baseUrl ?>/assets/plugins/morris/morris.min.js"></script>
    <script src="<?php echo $this->theme->baseUrl ?>/assets/plugins/raphael/raphael.min.js"></script>
    <script src="<?php echo $this->theme->baseUrl ?>/assets/js/chart.js"></script>

    <!-- Custom JS -->
    <script src="<?php echo $this->theme->baseUrl ?>/assets/js/app.js"></script>

</body>

</html>