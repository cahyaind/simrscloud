<div class="sidebar" id="sidebar">
    <div class="sidebar-inner slimscroll">
        <div id="sidebar-menu" class="sidebar-menu">
            <ul>
                <li class="menu-title">
                    <span>Main</span>
                </li>
                <li class="">
                    <!--Link utk liat icons https://themewagon.github.io/Ready-Bootstrap-Dashboard/icons.html -->
                    <a href="/site/index"><i class="la la-home"></i> <span> Home</span>
                        <span class=""></span></a>
                </li>
                <li class="submenu">
                    <a href="#"><i class="la la-book"></i> <span> Registrasi</span> <span class="menu-arrow"></span></a>
                    <ul style="display: none;">
                        <li><a href="/registrasi/main/index">Pendaftaran Pasien</a></li>
                        <li><a href="/registrasi/main/patactive">Rawat Jalan / IGD</a></li>
                        <li><a href="/registrasi/inap/pactive">Rawat Inap</a></li>
                    </ul>
                </li>
                <li class="submenu">
                    <a href="clients.html"><i class="la la-money"></i> <span>Finance</span> <span class="menu-arrow"></span></a>
                    <ul style="display: none;">
                        <li><a href="/finance/coacategory/index">COA Category</a></li>
                        <li><a href="/finance/coa/index">Charts Of Accounts</a></li>
                        <li><a href="/finance/product/index">Product</a></li>
                        <li><a href="/finance/rate/index">Rate</a></li>
                        <li><a href="/finance/billing/index">Billing</a></li>
                    </ul>
                </li>
                <li class="submenu">
                    <a href="#"><i class="la la-suitcase"></i> <span> Pharmacy</span> <span class="menu-arrow"></span></a>
                    <ul style="display: none;">
                        <li class="submenu">
                            <a href="#"><span> Setting</span> <span class="menu-arrow"></span></a>
                            <ul style="display: none;">
                                <li><a href="/pharmacy/invgroup/index">Inv Group</a></li>
                                <li><a href="/pharmacy/supplier/index">Supplier</a></li>
                                <li><a href="/pharmacy/manufactory/index">Manufactory</a></li>
                                <li><a href="/pharmacy/invlargeunit/index">Large Unit</a></li>
                                <li><a href="/pharmacy/invsmallunit/index">Small Unit</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li class="menu-title">
                    <span>Others</span>
                </li>
                <li class="submenu">
                    <a href="#"><i class="la la-files-o"></i> <span> Master Data </span> <span class="menu-arrow"></span></a>
                    <ul style="display: none;">
                        <li><a href="/masterdata/user/index">User</a></li>
                        <li><a href="/masterdata/employee/index">Employee</a></li>
                        <li><a href="/masterdata/job/index">Job</a></li>
                        <li><a href="/masterdata/unit/index">Unit</a></li>
                        <li><a href="/masterdata/doctor/index">Doctor</a></li>
                        <li><a href="/masterdata/patient/index">Patient</a></li>
                        <li><a href="/masterdata/patientrate/index">Patient Rate</a></li>
                        <li><a href="/masterdata/patientguaranty/index">Patient Guaranty</a></li>
                        <li><a href="/masterdata/guaranty/index">Guaranty</a></li>
                        <li><a href="/masterdata/reference/index">Reference</a></li>
                        <li><a href="/masterdata/classroom/index">Class Room</a></li>
                        <li><a href="/masterdata/clusterroom/index">Cluster Room</a></li>
                        <li><a href="/masterdata/room/index">Room</a></li>
                        <li><a href="/masterdata/bed/index">Bed</a></li>
                    </ul>
                </li>
                <li class="submenu">
                    <a href="#"><i class="la la-cube"></i> <span> System Default </span> <span class="menu-arrow"></span></a>
                    <ul style="display: none;">
                        <li><a href="/sysdef/gender/index">Gender</a></li>
                        <li><a href="/sysdef/religion/index">Religion</a></li>
                        <li><a href="/sysdef/education/index">Education</a></li>
                        <li><a href="/sysdef/ethnic/index">Ethnic</a></li>
                        <li><a href="/sysdef/bloodtype/index">Blood Type</a></li>
                        <li><a href="/sysdef/province/index">Province</a></li>
                        <li><a href="/sysdef/district/index">District</a></li>
                        <li><a href="/sysdef/subdistrict/index">Subdistrict</a></li>
                        <li><a href="/sysdef/hospital/index">Hospital</a></li>
                        <li><a href="/sysdef/maritalstatus/index">Marital Status</a></li>
                        <li><a href="/sysdef/unitgroup/index">Unit Group</a></li>
                        <li><a href="/sysdef/patientrategroup/index">Patient Rate Group</a></li>
                        <li><a href="/sysdef/emergency/index">Emergency</a></li>
                        <li><a href="/sysdef/emergencyreason/index">Emergency Reason</a></li>
                        <li><a href="/sysdef/picrelation/index">Pic Relation</a></li>
                        <li><a href="/sysdef/registrationstatus/index">Registration Status</a></li>
                        <li><a href="/sysdef/inpatientsource/index">Reg Inpatient Source</a></li>
                    </ul>
                </li>
                <li class="submenu">
                    <a href="#"><i class="la la-ellipsis-h"></i> <span> RBAC </span> <span class="menu-arrow"></span></a>
                    <ul style="display: none;">
                        <li><a href="/admin/role"> Role-Based Access Control </a></li>
                        <li><a href="/gii"> Gii </a></li>
                    </ul>
                </li>

            </ul>
        </div>
    </div>
</div>