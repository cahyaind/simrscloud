<?php

namespace app\modules\finance\controllers;

use Yii;
use app\models\RegistrationSearch;

class BillingController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $searchModel = new RegistrationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, null);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}
