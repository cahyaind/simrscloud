<?php

namespace app\modules\finance\controllers;

use app\models\ProductRate;
use app\models\ProductRateSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RateController implements the CRUD actions for ProductRate model.
 */
class RateController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all ProductRate models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProductRateSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ProductRate model.
     * @param int $prdrate_id Prdrate ID
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($prdrate_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($prdrate_id),
        ]);
    }

    /**
     * Creates a new ProductRate model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ProductRate();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'prdrate_id' => $model->prdrate_id]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing ProductRate model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $prdrate_id Prdrate ID
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($prdrate_id)
    {
        $model = $this->findModel($prdrate_id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'prdrate_id' => $model->prdrate_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ProductRate model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $prdrate_id Prdrate ID
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($prdrate_id)
    {
        $this->findModel($prdrate_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ProductRate model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $prdrate_id Prdrate ID
     * @return ProductRate the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($prdrate_id)
    {
        if (($model = ProductRate::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
