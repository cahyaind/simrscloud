<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ProductSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'product_id') ?>

    <?= $form->field($model, 'hospital_id') ?>

    <?= $form->field($model, 'unitgroup_id') ?>

    <?= $form->field($model, 'coa_id') ?>

    <?= $form->field($model, 'name') ?>

    <?php // echo $form->field($model, 'is_active')->checkbox() 
    ?>

    <?php // echo $form->field($model, 'created_by') 
    ?>

    <?php // echo $form->field($model, 'created_time') 
    ?>

    <?php // echo $form->field($model, 'updated_by') 
    ?>

    <?php // echo $form->field($model, 'updated_time') 
    ?>

    <?php // echo $form->field($model, 'is_deleted')->checkbox() 
    ?>

    <?php // echo $form->field($model, 'deleted_by') 
    ?>

    <?php // echo $form->field($model, 'deleted_time') 
    ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>