<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProductRate */

$this->title = 'Update Product Rate: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Product Rates', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'prdrate_id' => $model->prdrate_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="product-rate-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>